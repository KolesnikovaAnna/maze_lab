from src import maze
import unittest

class Test(unittest.TestCase):

    def test1(self):
        matrix = [[1, 1, 0, 1, 1], [1, 0, 0, 0, 1],
                  [0, 0, 1, 0, 1], [1, 1, 0, 0, 0], [1, 1, 0, 1, 1]]
        self.assertEqual(maze.find_way(matrix), 4)


    def test2(self):
        matrix = [[1, 1, 1, 1, 1], [1, 1, 1, 0, 1],
                  [1, 1, 1, 1, 1], [1, 1, 1, 1, 1], [1, 1, 1, 1, 1]]
        self.assertEqual(maze.find_way(matrix), -1)
