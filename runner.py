import unittest
import test_module

TestSuite = unittest.TestSuite()
TestSuite.addTest(unittest.makeSuite(test_module.Test))
runner = unittest.TextTestRunner()
runner.run(TestSuite)
