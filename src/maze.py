def find_way(M):
    x, y, steps = 1, 3, 0
    while True:
        if x == 0 or y == 0 or x == len(M)-1 or y == len(M)-1:
            break
        elif M[x+1][y] == 0:
            x = x + 1
            steps += 1
        elif M[x][y-1] == 0:
            y = y - 1
            steps += 1
        elif M[x][y+1] == 0:
            y = y + 1
            steps += 1
        elif M[x-1][y] == 0:
            x = x - 1
            steps += 1
        else:
            return -1
    return steps
